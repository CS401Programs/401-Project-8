import java.io.*;
import java.util.*;

public class Jumbles {

	private static ArrayList<String> dictionary = new ArrayList<String>();
	private static ArrayList<String> jumbles = new ArrayList<String>();
	private static ArrayList<String> answerKey = new ArrayList<String>();
	
	public static void main(String[] args) throws IOException {
		
		// Check to see if the user gave the proper arguments
		if (args.length < 1 )
		{
			System.out.println("\nusage: C:\\> java Jumbles dictionary.txt jumbles.txt\n\n");
			System.exit(0);
		}

		// The steps to solve the word jumble
		// are broken up into 3 method calls
		buildList(args);
		buildKey();
		printKey();
	}
	
	// This method will build your dictionary and jumbles ArrayList
	private static void buildList(String args[]) throws IOException {
		
		BufferedReader dictReader = new BufferedReader(new FileReader(args[0]));
		BufferedReader jumblesReader = new BufferedReader(new FileReader(args[1]));
		
		while (dictReader.ready())
			dictionary.add(dictReader.readLine());
		dictReader.close();
		
		while (jumblesReader.ready())
			jumbles.add(jumblesReader.readLine());
		jumblesReader.close();
		
		Collections.sort(dictionary);
		Collections.sort(jumbles);
	}
	
	// This method will build your answer key ArrayList
	private static void buildKey() {
		
		for (String dWord : dictionary) {
			String dCanon = toCanon(dWord);
			int index = binarySearch(answerKey, dCanon + " ");
			
			if (index < 0) {
				index = -(index + 1);
				answerKey.add(index, dCanon + " " + dWord);
			}
			
			else
				answerKey.set(index, answerKey.get(index) + " " + dWord);
		}
	}
	
	// This method will print the answer
	// key and the words that can be made
	// from a jumble in 2 formatted columns
	private static void printKey() {
		
		for (String jWord : jumbles) {
            System.out.print(jWord);
             
            String jCanon = toCanon(jWord);
            int index = binarySearch(answerKey, jCanon + " ");
             
            if (index >= 0)
                System.out.println(answerKey.get(index).substring(answerKey.get(index).indexOf(" ")));
            else
                System.out.println();
        }
	}
	
	// This method will return the canonical
	// form of the string that was passed in
	private static String toCanon(String s) {
		
		char[] letters = s.toCharArray();
		Arrays.sort(letters);
		return new String(letters);
	}
	
	// A standard binary search method modified
	// for an insert in order algorithm
	private static int binarySearch(ArrayList<String> aList, String key) {
		
		int start = 0;
		int end = aList.size() - 1;
		int mid;
		
		while (start <= end) {
			mid = (start + end) / 2;
			
			if (aList.get(mid).startsWith(key))
				return mid;
			else if (aList.get(mid).compareTo(key) > 0)
				end = mid - 1;
			else if (aList.get(mid).compareTo(key) < 0)
				start = mid + 1;
		}
		return -(start + 1);
	}
}
